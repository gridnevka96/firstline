def time_convert(seconds):
    if seconds is None or not isinstance(seconds, int):
        return "Секунды должны быть int"
    if seconds < 0:
        return "Секунды не могут быть отрицательными"

    time_units = [
        ("year", 60 * 60 * 24 * 365, "s"),
        ("day", 60 * 60 * 24, "s"),
        ("hour", 60 * 60, "s"),
        ("minute", 60, "s"),
        ("second", 1, "s")
        ]

    components = []
    for unit, value, suffix in time_units:
        if seconds >= value:
            count = int(seconds / value)
            seconds -= count * value
            components.append(f"{count} {unit}{'' if count==1 else suffix}")

    if not components:
        return "0 seconds"
    elif len(components) == 1:
        return components[0]
    else:
        return ", ".join(components[:-1]) + f" and {components[-1]}"