def dig_pow(n, p):
    # получаем список цифр числа n
    digits = [int(d) for d in str(n)]
    # вычисляем сумму цифр, возведенных в степень p
    total = sum([d ** (p + i) for i, d in enumerate(digits)])
    # проверяем, делится ли результат на n без остатка
    if total % n == 0:
        return total // n
    else:
        return -1